$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "mps7"

require "minitest/autorun"
require "minitest/pride"

module TestHelpers
  def new_record(attrs = {})
    defaults = { type: :credit, timestamp: Time.now.to_i, user_id: 1234, dollars: 123.45 }
    MPS7::Record.new(defaults.merge(attrs))
  end
end

module Minitest
  class Test
    include TestHelpers
  end
end
