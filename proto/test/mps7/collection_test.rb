require "test_helper"

class MPS7::CollectionTest < Minitest::Test
  def test_add_record
    coll = MPS7::Collection.new
    assert_equal 0, coll.size

    coll.add(new_record)
    assert_equal 1, coll.size
  end

  def test_dollars
    coll = MPS7::Collection.new

    coll.add(new_record(dollars: 10.0, type: :debit))
    coll.add(new_record(dollars: 20.0, type: :debit))
    coll.add(new_record(dollars: 30.0, type: :credit))
    coll.add(new_record(dollars: 40.0, type: :credit))
    coll.add(new_record(dollars: 40.0, type: :start_autopay)) # should ignore

    assert_equal 100.00, coll.dollars
    assert_equal 30.00, coll.dollars(type: :debit)
    assert_equal 70.00, coll.dollars(type: :credit)
  end

  def test_count
    coll = MPS7::Collection.new

    coll.add(new_record(type: :debit))
    coll.add(new_record(type: :credit))
    coll.add(new_record(type: :credit))
    coll.add(new_record(type: :end_autopay))
    coll.add(new_record(type: :end_autopay))
    coll.add(new_record(type: :start_autopay))
    coll.add(new_record(type: :start_autopay))
    coll.add(new_record(type: :start_autopay))

    assert_equal 8, coll.count
    assert_equal 1, coll.count(type: :debit)
    assert_equal 2, coll.count(type: :credit)
    assert_equal 2, coll.count(type: :end_autopay)
    assert_equal 3, coll.count(type: :start_autopay)
  end

  def test_user
    coll = MPS7::Collection.new

    coll.add(new_record(user_id: 1))
    coll.add(new_record(user_id: 2))
    coll.add(new_record(user_id: 1))
    coll.add(new_record(user_id: 3))

    assert_equal 2, coll.account(1).size
    assert_equal 1, coll.account(2).size
    assert_equal 1, coll.account(3).size
  end

  def test_balance
    coll = MPS7::Collection.new

    coll.add(new_record(dollars: 10.0, type: :debit))
    coll.add(new_record(dollars: 20.0, type: :debit))
    coll.add(new_record(dollars: 30.0, type: :credit))
    coll.add(new_record(dollars: 40.0, type: :credit))

    assert_equal 40.00, coll.balance
  end
end
