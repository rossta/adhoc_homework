require "test_helper"

class MPS7::RecordTest < Minitest::Test

  def test_debit
    record = new_record(type: :debit)

    assert_equal 0, record.type
    assert record.transaction?
  end

  def test_credit
    record = new_record(type: :credit)

    assert_equal 1, record.type
    assert record.transaction?
  end

  def test_start_autopay
    record = new_record(type: :start_autopay)

    assert_equal 2, record.type
    refute record.transaction?
  end

  def test_end_autopay
    record = new_record(type: :end_autopay)

    assert_equal 3, record.type
    refute record.transaction?
  end

  def test_format_dollars
    assert_equal "123.45", new_record(dollars: 123.45).format_dollars
    assert_equal "0.00", new_record(dollars: nil).format_dollars
  end
end
