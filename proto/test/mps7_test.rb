require "test_helper"

class MPS7::RecordTest < Minitest::Test
  def test_parse_returns_collection
    collection = MPS7.parse(File.expand_path("../../txnlog.dat", __FILE__))

    assert_equal 72, collection.size
  end

  def test_known_user_id
    known_user_id = 2456938384156277127

    collection = MPS7.parse(File.expand_path("../../txnlog.dat", __FILE__))

    assert_equal 2, collection.account(known_user_id).count
    assert_equal 0.0, collection.account(known_user_id).balance
  end
end
