module MPS7
  require "mps7/record"
  require "mps7/collection"
  require "mps7/parser"

  # Parse the given file in MPS7 format
  #
  # @param file_path [String] path to file
  # @return [MPS7::Collection] collection of MPS7 records
  def self.parse(file_path)
    MPS7::Parser.new.parse(file_path)
  end
end
