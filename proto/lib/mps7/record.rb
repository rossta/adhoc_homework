module MPS7
  class Record
    TYPES = {
      debit: DEBIT = 0,
      credit: CREDIT = 1,
      start_autopay: START_AUTOPAY = 2,
      end_autopay: END_AUTOPAY = 3
    }

    def self.type_names
      TYPES.keys
    end

    attr_accessor :type, :timestamp, :user_id, :dollars
    def initialize(type:, timestamp:, user_id:, dollars: nil)
      @type = cast_type(type)
      @timestamp = timestamp
      @user_id = user_id
      @dollars = dollars
    end

    def type_name
      TYPES.key(type)
    end

    def transaction?
      @type == DEBIT || @type == CREDIT
    end

    def format_dollars
      @dollars ? '%.2f' % @dollars : "0.00"
    end

    def to_s
      "<Record: type:#{@type}, timestamp:\"#{@timestamp}\" user_id:#{@user_id} dollars: $#{format_dollars}>"
    end

    private

    def cast_type(type)
      TYPES.fetch(type, type)
    end
  end
end
