module MPS7
  class Parser
    BYTE_LENGTHS = [
      MAGIC_BYTES = 4,
      VERSION_BYTES = 1,
      SIZE_BYTES = 4,
      TYPE_BYTES = 1,
      TIME_BYTES = 4,
      USER_BYTES = 8,
      DOLLAR_BYTES = 8
    ]

    def parse(file_path)
      collection = nil
      File.open(file_path) do |file|
        raise "Unknown format" unless file.read(MAGIC_BYTES) == "MPS7"

        _version = uint8(file.read(VERSION_BYTES))
        _size = uint32_network(file.read(SIZE_BYTES))

        collection = MPS7::Collection.new

        while first_byte = file.read(TYPE_BYTES) do
          type = uint8(first_byte)
          timestamp = uint32_network(file.read(TIME_BYTES))
          user_id = uint64_network(file.read(USER_BYTES))

          record = Record.new(
            type: type,
            timestamp: Time.at(timestamp),
            user_id: user_id
          )

          record.dollars = float64(file.read(DOLLAR_BYTES)) if record.transaction?

          puts record if ENV['DEBUG']

          collection.add(record)
        end
      end

      collection
    end

    private

    # Used to determine whether machine's native endian encoding
    # is big endian (as opposed to little endian)
    def big_endian?
      [1].pack("s") == [1].pack("n")
    end

    def uint8(str)
      str.unpack("C").last
    end

    def uint32_network(str)
      str.unpack("N").last
    end

    # Ruby is missing a big endian string template for uint64
    # so we reverse the byte order on little-endian machines first
    def uint64_network(str)
      str = big_endian? ? str : str.reverse
      str.unpack("Q").last
    end

    def float64(str)
      str.unpack("G*").last
    end
  end
end
