module MPS7
  class Collection
    def initialize(records: [])
      @records = records
    end

    def add(record)
      @records << record
    end

    def dollars(type: [:credit, :debit])
      types = Array(type)
      @records.select { |r| types.include?(r.type_name) }.map(&:dollars).reduce(&:+)
    end

    def count(type: Record.type_names)
      types = Array(type)
      @records.count { |r| types.include?(r.type_name) }
    end
    alias size count

    def balance
      dollars(type: :credit) - dollars(type: :debit)
    end

    def account(user_id)
      self.class.new records: @records.select { |r| r.user_id == user_id }
    end
  end
end
