require 'csv'
require 'set'

require "pry"

module RateArea
  def area_hash
    { state: state, rate_area: rate_area }
  end
end

# Represents the mapping of a zipcode to a state and rate_area. There
# may many ZipAreas for a given zipcode. Struct arguments map directly
# to CSV headers given by zips.csv
#
ZipArea = Struct.new(:zipcode, :state, :fips, :name, :rate_area) do
  include RateArea
end

# Represents CSV data from headers in plans.csv for determining rate for
# a given plan ID in a state and rate area.
#
Plan = Struct.new(:plan_id, :state, :metal_level, :rate, :rate_area) do
  include RateArea
end

# Calculate SLCSP for zipcodes in given slcsp.csv from source data
# in zips.csv and plans.csv. Provides options for setting source files.
#
# @param opts [Hash] options for setting file paths
#
# Example:
#
# SLCSP.new.process!
#
class SLCSP
  def initialize(opts = {})
    @slcsp_file = opts.fetch(:src_file, file_path("../slcsp.csv"))
    @zips_file = opts.fetch(:zip_file, file_path("../zips.csv"))
    @plans_file = opts.fetch(:plans_file, file_path("../plans.csv"))
    @output_file = opts.fetch(:output_file, @slcsp_file)
    @debug = opts.fetch(:debug, false)
  end

  def process!
    reset!

    extract_zipcodes
    extract_zip_areas
    extract_plans

    write_output_file
  end

  private

  def reset!
    @zipcodes = []
    @zips = []
    @areas = {}
    @plans = []
    @silver_area_rates = {}
    @missing_rates = {}
  end

  def write_output_file
    CSV.open(@output_file, "w") do |dest|
      dest << %w[zipcode rate]

      @zipcodes.each do |zipcode|
        slcsp = nil
        zip_areas = @areas[zipcode]
        area_count = zip_areas.size

        if area_count == 1
          zip_area = zip_areas.first
          rates = Array(@silver_area_rates[zip_area.area_hash]).sort
          slcsp = rates[1] # second lowest

          warn_missing_state(zipcode, zip_area[:state]) if @debug && rates.empty?
        else
          warn_too_many_areas(zipcode, area_count) if @debug
        end

        dest << [zipcode, slcsp]
      end
    end
  end

  def extract_zipcodes
    CSV.foreach(@slcsp_file, headers: true) do |src|
      @zipcodes << src["zipcode"]
    end
  end

  def extract_zip_areas
    CSV.foreach(@zips_file, headers: true) do |csv|
      zip_area = ZipArea.new(*csv.fields)
      @areas[zip_area.zipcode] ||= []
      @areas[zip_area.zipcode] << zip_area
    end
  end

  def extract_plans
    CSV.foreach(@plans_file, headers: true) do |csv|
      plan = Plan.new(*csv.fields)
      @plans << plan

      next unless plan.metal_level.downcase == "silver"

      @silver_area_rates[plan.area_hash] ||= []
      @silver_area_rates[plan.area_hash] << plan.rate
    end
  end

  def warn_missing_state(zip, state)
    plans_by_state = @plans.select { |p| p.state == state }
    if plans_by_state.empty?
      warn "No plan found for zip #{zip} because no plan data for #{state}"
    else
      warn "No reason found for zip #{zip}"
    end
  end

  def warn_too_many_areas(zip, count)
    warn "No plan found for zip #{zip} because there are not 1 but #{count} rate areas"
  end

  def file_path(path)
    File.expand_path(path, __FILE__)
  end
end

if __FILE__ == $0
  # Usage:
  #   $ ruby slcsp.rb
  #   $ ruby slcsp.rb outputfile.csv
  #
  output_file = ARGV.shift || "./slcsp-results-#{Time.now.to_i}.csv"
  SLCSP.new(output_file: output_file, debug: true).process!

  exec "diff slcsp.csv #{output_file}"
end
