package main

import (
	"crypto/sha1"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"sort"
	"strconv"
	"strings"
)

const (
	crlf       = "\r\n"
	colonspace = ": "
	semicolon  = ";"
)

func ChecksumMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rec := httptest.NewRecorder()
		h.ServeHTTP(rec, r)

		canonical := canonicalResponse(rec)
		checksum := stringAsChecksum(canonical)

		w.Header().Set("X-Checksum", checksum)
		h.ServeHTTP(w, r)
	})
}

func stringAsChecksum(raw string) string {
	hasher := sha1.New()
	bytes := []byte(raw)
	hasher.Write(bytes)
	return hex.EncodeToString(hasher.Sum(nil))
}

func canonicalResponse(rec *httptest.ResponseRecorder) string {
	headerMap := rec.Header()
	names := sortedKeys(headerMap)

	raw := strconv.Itoa(rec.Code) + crlf
	for _, k := range names {
		raw = raw + k + colonspace + strings.Join(headerMap[k], "") + crlf
	}
	raw = raw + "X-Checksum-Headers" + colonspace + strings.Join(names, semicolon)
	raw = raw + crlf + crlf
	return raw + rec.Body.String()
}

func sortedKeys(m map[string][]string) []string {
	keys := make([]string, len(m))
	i := 0
	for k, _ := range m {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	return keys
}

// Do not change this function.
func main() {
	var listenAddr = flag.String("http", ":8080", "address to listen on for HTTP")
	flag.Parse()

	http.Handle("/", ChecksumMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Date", "Sun, 08 May 2016 14:04:53 GMT")
		msg := "Curiosity is insubordination in its purest form.\n"
		w.Header().Set("Content-Length", strconv.Itoa(len(msg)))
		fmt.Fprintf(w, msg)
	})))

	log.Fatal(http.ListenAndServe(*listenAddr, nil))
}
