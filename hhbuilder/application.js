(function() {
  var state = {};
  state.handlers = {};
  state.attributeNames = ['age', 'rel', 'smoker'];
  state.validations = {
    "[name='age']": validateAge,
    "[name='rel']": validateRelationship
  };
  state.id = 0;
  state.people = new PersonCollection();

  function Person(attrs) {
    attrs = attrs || {};
    this.age = attrs.age;
    this.rel = attrs.rel;
    this.smoker = attrs.smoker;
    this.id = ++state.id;
  }

  Person.prototype.toString = function() {
    var attributes = [];
    var json = this.toJSON();
    for (var attr in json) {
      if (json.hasOwnProperty(attr)) {
        attributes.push(attr + ": " + json[attr]);
      }
    }
    return "Person - " + attributes.join("; ");
  };

  Person.prototype.toJSON = function toJSON() {
    return {
      id: this.id,
      age: this.age,
      rel: this.rel,
      smoker: this.smoker,
    };
  };

  function PersonCollection() {
    this.people = {};
  }

  PersonCollection.prototype.add = function(person) {
    this.people[person.id] = person;
    return person;
  };

  PersonCollection.prototype.remove = function(id) {
    var person = this.people[id];
    delete this.people[id];
    return person;
  };

  PersonCollection.prototype.toJSON = function() {
    return Object.keys(this.people).map(function(id) {
      return this.people[id].toJSON();
    }.bind(this));
  };

  PersonCollection.prototype.forEach = function(callback) {
    for(var id in this.people) {
      if (this.people.hasOwnProperty(id)) {
        callback(this.people[id]);
      };
    }
  };

  function fakeSubmitPeople(people) {
    var debug = document.querySelector('.debug');

    debug.innerText = JSON.stringify(people);
    debug.style.display = "block";
  }

  function addPersonFromForm(form) {
    var isValid = validateForm.call(form);

    if (!isValid) {
      alert('Form is not valid');
      return false;
    }

    addPerson(extractFormAttributes(form));
    form.reset();
    return false;
  }

  function extractFormAttributes(form) {
    var age = form.querySelector("[name='age']");
    var rel = form.querySelector("[name='rel']");
    var smoker = form.querySelector("[name='smoker']")

    return {
      age: age.value,
      rel: rel.value,
      smoker: smoker.checked,
    };
  }

  function addPerson(attrs) {
    var person = new Person(attrs);
    state.people.add(person);
    renderHouseholdHtml(state.people);
    return person;
  }

  function removePerson(id) {
    state.people.remove(id);
    renderHouseholdHtml(state.people);
  }

  function renderHouseholdHtml(people) {
    var builder = document.querySelector('.builder');
    var household = document.querySelector('.household');
    var newHousehold = document.createElement('ol');
    newHousehold.setAttribute('class', 'household');

    people.forEach(function(person) {
      var li = createPersonElement(person);
      newHousehold.appendChild(li);
    });

    builder.replaceChild(newHousehold, household);
  }

  function createPersonElement(person) {
    var li = document.createElement('li');
    var span = document.createElement('span');
    var button = document.createElement('button');
    li.dataset.id = person.id;
    span.innerText = person.toString();
    button.setAttribute('class', 'remove');
    button.innerText = "remove";

    li.appendChild(span);
    li.appendChild(button);
    return li;
  }

  function createAttributeElement(name, value) {
    var span = document.createElement('span');
    span.setAttribute('class', 'attribute ' + name);
    span.innerText = name + ': ' + value + '; ';
    return span;
  }

  function validateForm() {
    var form = this;
    return Object.keys(state.validations).reduce(function(isValid, selector) {
      var el = form.querySelector(selector);
      return isValid && state.validations[selector].call(el);
    }, true);
  }

  function validateRelationship() {
    var isValid = this.value !== 'undefined' && this.value.length > 0;
    return validate.call(this, isValid, 'Relationship is required.');
  }

  function validateAge() {
    var age = parseInt(this.value, 10);
    var isValid = !isNaN(age) && age > 0;

    return validate.call(this, isValid, 'Age must be a number greater than zero.') &&
      validate.call(this, age < 130, 'Seriously?');
  }

  function validate(isValid, errorMsg) {
    if (!isValid) {
      console.warn('invalid', this, errorMsg);
      appendErrorHtml.call(this, this.parentElement, errorMsg);
      return false;
    }

    removeErrorHtml.call(this, this.parentElement);
    return true;
  }

  function appendErrorHtml(parent, errorMsg) {
    var el = parent.querySelector('.error');
    if (!el) {
      el = document.createElement('span');
      el.setAttribute('class', 'error');
      parent.appendChild(el);
    }
    el.innerText = errorMsg;
  }

  function removeErrorHtml(parent) {
    var el = parent.querySelector('.error');
    if (!el) {
      return;
    }
    el.innerText = '';
  }

  function on(selector, event, callback) {
    var events;
    state.handlers || (state.handlers = {});
    state.handlers[event] || (state.handlers[event] = []);
    events = state.handlers[event]
    state.handlers[event] = events.concat({
      selector: selector,
      callback: callback
    });
  }

  function getEventTarget(e) {
    var e = e || window.event;
    return e.target || e.srcElement;
  }

  function selectorMatches(el, selector) {
    var prototype = Element.prototype;
    var func = prototype.matches ||
      prototype.webkitMatchesSelector ||
      prototype.mozMatchesSelector ||
      prototype.msMatchesSelector ||
      function(s) {
        return ~[].indexOf.call(document.querySelectorAll(s), this);
      };
    return func.call(el, selector);
  }

  function delegateEventHandlers() {
    Object.keys(state.handlers).forEach(function(event) {
      window.addEventListener(event, function windowHandler(e) {
        var el = getEventTarget(e);
        var handlers = state.handlers;

        if (!(handlers && handlers[event])) {
          return;
        }

        (handlers[event] || []).forEach(function(handler) {
          if (selectorMatches(el, handler.selector)) {
            handler.callback.call(el, e);
          }
        })
      });
    });
  }

  function registerEventHandlers() {
    on('button.add', 'click', function(e) {
      e.preventDefault();
      var form = document.querySelector('form');
      addPersonFromForm(form);
    });

    on('form', 'submit', function(e) {
      e.preventDefault();
      fakeSubmitPeople(state.people);
      return false;
    });

    on('button.remove', 'click', function() {
      var li = this.parentElement;
      removePerson(li.dataset.id);
    });

    Object.keys(state.validations).forEach(function(selector) {
      on(selector, 'change', function(e) {
        return state.validations[selector].call(this);
      });
    });
  }

  registerEventHandlers();
  delegateEventHandlers();
})();
